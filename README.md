#软件基础作业

#### 作业描述

实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。

#### 使用说明

具体命令行界面要求如下：

命令模式： wc.exe [参数] [文件名]

例：wc.exe -c file.txt 统计字符数
    wc.exe -d file.txt 统计单词数
    wc.exe -e file.txt 统计句子数

#### 文件列表

1.  v0.1空代码
2.  v0.2基本功能代码
3.  v0.3拓展功能代码

#### 运行结果
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/164516_a4994458_9707930.png "单词数.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/164545_7549b80e_9707930.png "句子数.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/164607_dd8b32a7_9707930.png "字符数.PNG")